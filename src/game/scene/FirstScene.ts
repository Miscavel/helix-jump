import { 
    Vector3, 
    HemisphericLight, 
    MeshBuilder, 
    Mesh, 
    Scene, 
    KeyboardEventTypes,
    ArcRotateCamera,
    StandardMaterial,
    Color3,
    PhysicsImpostor,
    CannonJSPlugin,
    Animation,
    PointerEventTypes,
    Nullable,
    Observer,
    PointerInfo,
    Texture,
    TrailMesh,
    ParticleSystem,
    Color4,
    AssetsManager
} from '@babylonjs/core';
import {
    AdvancedDynamicTexture,
    TextBlock
} from '@babylonjs/gui';
import { setupCameraAutoAdjust, ADJUSTMENT } from '../util/CameraAdjustment';
import { GameScene } from '../interface';
import cannon from 'cannon';
import GameInstanceManager from '../manager/GameInstanceManager';

export default class FirstScene implements GameScene {
    properties = {
        base: {
            diameterTop: 3, 
            diameterBottom: 3, 
            arc: 1, 
            tessellation: 12, 
            height: 0.25, 
            sideOrientation: Mesh.DOUBLESIDE, 
            enclose: true
        },
        pole: {
            diameterTop: 1, 
            diameterBottom: 1, 
            arc: 1, 
            tessellation: 12, 
            height: 20, 
            sideOrientation: Mesh.DOUBLESIDE, 
            enclose: true
        }
    };

    platformCount = 10;

    gap = (this.properties.pole.height) / (this.platformCount + 1);

    ballPositioningRadius = (this.properties.base.diameterTop + this.properties.pole.diameterTop) / 4;

    currentBallAngle = 90 / 180 * Math.PI;

    previousAlpha?: number;

    camera?: ArcRotateCamera;

    gameOver = false;

    ballDiameter = 0.3;

    speedLimit = {
        min: -8,
        max: 6.5
    }

    touching = false;

    touchControlObservable?: Nullable<Observer<PointerInfo>>;

    textures: { [key: string]: Texture } = {};

    /**
     * Scene create function
     * @param scene Scene object that has been registered to Babylon engine
     */
    public onSceneReady = (scene: Scene) => {
        const camera = new ArcRotateCamera(
            "Camera",
            0,
            0,
            0,
            new Vector3(0, 15, 0),
            scene
        );
        this.loadTextures(scene, camera);
    }

    initializeScene(scene: Scene, camera: ArcRotateCamera) {
        scene.enablePhysics(new Vector3(0, -20, 0), new CannonJSPlugin(undefined, undefined, cannon));

        camera.setPosition(new Vector3(0, 20, 7.5));
        camera.target.y = this.platformCount * this.gap;

        this.camera = camera;
        
        const light1 = new HemisphericLight(
            "light1",
            new Vector3(0, 1, 0),
            scene
        );

        const ball = MeshBuilder.CreateSphere("ball", {diameter: this.ballDiameter, segments: 4}, scene);
        ball.position.z = this.ballPositioningRadius;
        ball.position.y = 20;
        ball.metadata = {
            x: ball.position.x,
            z: ball.position.z
        };
        ball.physicsImpostor = new PhysicsImpostor(ball, PhysicsImpostor.SphereImpostor, {mass: 3});
        ball.physicsImpostor.friction = 0;
        ball.physicsImpostor.executeNativeFunction(function (world, body) {
            body.fixedRotation = true;
            body.updateMassProperties();
        });
        ball.physicsImpostor.registerAfterPhysicsStep((impostor) => {
            ball.physicsImpostor?.executeNativeFunction((world, body) => {
                const { velocity } = body;
                body.velocity = new cannon.Vec3(
                    velocity.x,
                    Math.min(
                        Math.max(velocity.y, this.speedLimit.min),
                        this.speedLimit.max
                    ), 
                    velocity.z
                );
            });
        });

        const blueMat = new StandardMaterial("blueMat", scene);
        blueMat.diffuseColor = new Color3(0, 0, 1);

        ball.material = blueMat;

        const trail = new TrailMesh("ballTrail", ball, scene, 0.1, 15);
        const trailMat = new StandardMaterial("trailMat", scene);
        trailMat.emissiveColor = trailMat.diffuseColor = Color3.Blue();
        trailMat.specularColor = Color3.Black();
        trail.material = trailMat;

        const particleSystem = new ParticleSystem("particles", 2000, scene);
        particleSystem.particleTexture = new Texture("/helix-jump/flare.png", scene);
        particleSystem.gravity = new Vector3(0, -40, 0);
        
        particleSystem.minEmitPower = particleSystem.maxEmitPower = 1.0;
        particleSystem.minSize = particleSystem.maxSize = 0.06;
        particleSystem.minLifeTime = particleSystem.maxLifeTime = 0.3;

        particleSystem.minEmitBox = new Vector3(.1, 0, 0);
        particleSystem.maxEmitBox = new Vector3(-.1, 0, 0);
        particleSystem.direction1 = new Vector3(4, 3.5, 4);
        particleSystem.direction2 = new Vector3(-4, 3.5, -4);

        particleSystem.color1 = new Color4(1, 1, 1.0, 1.0);
        particleSystem.color2 = new Color4(1, 1, 1.0, 1.0);
        particleSystem.colorDead = new Color4(1, 1, 1.0, 1.0);

        particleSystem.blendMode = ParticleSystem.BLENDMODE_STANDARD;
        
        particleSystem.emitter = ball;

        particleSystem.manualEmitCount = 0;
        particleSystem.start();
        ball.metadata.particleSystem = particleSystem;

        this.previousAlpha = camera.alpha;
        scene.registerBeforeRender(() => {
            ball.position.x = ball.metadata.x;
            ball.position.z = ball.metadata.z;

            if (ball.position.y - 1.5 < camera.target.y) {
                camera.target.y = ball.position.y - 1.5;
            }
        });

        this.generatePlatform(
            scene, 
            ball,
            { min: 1, max: 3 }
        );

        /**
         * Sets auto-adjust on camera to ensure that all objects have the same size with 
         * respect to screen size
         */
        setupCameraAutoAdjust(scene, camera, ADJUSTMENT.VERTICAL);
        this.setupControl(scene, ball);
        // this.setupGUI(scene);
    }

    loadTextures(scene: Scene, camera: ArcRotateCamera) {
        const assetsManager = new AssetsManager(scene);

        assetsManager.addTextureTask("decalTextureLoad", "/helix-jump/decal.png").onSuccess = (task) => {
            this.textures.decal = task.texture;
        };

        assetsManager.addTextureTask("particleTextureLoad", "/helix-jump/flare.png").onSuccess = (task) => {
            this.textures.particle = task.texture;
        };

        assetsManager.load();

        assetsManager.onFinish = (tasks) => {
            this.initializeScene(scene, camera);
        };
    }

    generatePlatform(
        scene: Scene, 
        ball: Mesh,
        holeCount = { min: 1, max: 1 },
        holeArcLimit = { min: 0.1, max: 0.25 },
        platformArcLimit = { min: 0.075 },
        dangerChance = 0.1,
        dangerCountLimit = 3,
        dangerArcLimit = { min: 0.05, max: 0.2 }
    ) {
        const base = MeshBuilder.CreateCylinder("base", this.properties.base, scene);
        const pole = MeshBuilder.CreateCylinder("pole", this.properties.pole, scene);
        pole.position.y = base.position.y + (this.properties.base.height + this.properties.pole.height) / 2;
        
        base.physicsImpostor = new PhysicsImpostor(base, PhysicsImpostor.CylinderImpostor, {mass: 0});

        ball.physicsImpostor?.registerOnPhysicsCollide(base.physicsImpostor, () => {
            if (!this.gameOver) {
                ball.physicsImpostor?.dispose();
                ball.position.y = base.position.y + this.properties.base.height / 2 + this.ballDiameter / 2;
                this.setupGUI(scene, "Victory! Tap to restart");
                this.gameOver = true;
            }
        });

        const platformPosition = {
            x: base.position.x,
            y: base.position.y + this.gap
        };

        const redMat = new StandardMaterial("redMat", scene);
        redMat.diffuseColor = new Color3(1, 0, 0);

        const greenMat = new StandardMaterial("greenMat", scene);
        greenMat.diffuseColor = new Color3(0, 1, 0);

        const blueMat = new StandardMaterial("blueMat", scene);
        blueMat.diffuseColor = new Color3(0, 0, 1);

        const decalMat = new StandardMaterial("decalMat", scene);
        decalMat.diffuseTexture = new Texture("/helix-jump/decal.png", scene);
        decalMat.opacityTexture = new Texture("/helix-jump/decal.png", scene);
		decalMat.diffuseTexture.hasAlpha = true;
        decalMat.zOffset = -2;
        
        const decalFadeAnim = new Animation("disappearAnimFade", "visibility", 30,  Animation.ANIMATIONTYPE_FLOAT);
        decalFadeAnim.setKeys([
            {
                frame: 0,
                value: 1
            },
            {
                frame: 60,
                value: 0
            }
        ]);

        for (let i = 0; i < this.platformCount; i++) {
            const platformGroup: Array<Mesh> = [];
            let remainingArc = 1;

            /**
             * Generate hole arcs
             */
            const holeArcs = [];
            const holeCountLimit = Math.round(this.RNGBetween(holeCount.min, holeCount.max, 3));
            for (let i = 0; i < holeCountLimit; i++) {
                const arc = this.RNGBetween(holeArcLimit.min, holeArcLimit.max, 2);
                holeArcs.push(arc);
                
                remainingArc -= arc;
            }

            /**
             * Generate platform arcs
             */
            const platformArcs = [];
            for (let i = 0; i < holeCountLimit; i++) {
                const max = remainingArc / (holeCountLimit - i);
                
                const arc = this.RNGBetween(platformArcLimit.min, max, 2);
                platformArcs.push(arc);

                remainingArc -= arc;
            }
            if (remainingArc > 0) {
                platformArcs[platformArcs.length - 1] += remainingArc;
            } else if (remainingArc < 0) {
                console.log(remainingArc);
            }

            let totalArc = 0;
            let dangerCount = 0;
            while (holeArcs.length > 0 || platformArcs.length > 0) {
                const holeArc = holeArcs.pop();
                let platformArc = platformArcs.pop();

                if (holeArc) {
                    const hole = MeshBuilder.CreateCylinder(
                        "hole", 
                        {...this.properties.base, arc: holeArc, height: this.properties.base.height / 2}, 
                        scene
                    );
                    hole.position.y = platformPosition.y - this.properties.base.height / 2;
                    hole.material = blueMat;

                    hole.metadata = { type: "hole", active: true, arc: - totalArc - holeArc / 2 };

                    hole.rotate(new Vector3(0, 1, 0), totalArc * Math.PI * 2);

                    platformGroup.push(hole);

                    totalArc += holeArc;

                    hole.visibility = 0;
                }
                
                if (platformArc) {
                    while (platformArc > 0) {
                        let partitionArc = Math.min(
                            this.RNGBetween(dangerArcLimit.min, dangerArcLimit.max, 2),
                            platformArc
                        );

                        if (platformArc - partitionArc < dangerArcLimit.min) partitionArc = platformArc;

                        const platform = MeshBuilder.CreateCylinder(
                            "platform", 
                            {...this.properties.base, arc: partitionArc}, 
                            scene
                        );
                        platform.position.y = platformPosition.y;
                        
                        if (!platform.metadata) {
                            platform.metadata = { active: true, arc: - totalArc - partitionArc / 2 };
                        }

                        if (Math.random() < dangerChance && dangerCount < dangerCountLimit) {
                            platform.material = redMat;
                            platform.metadata.type = "danger";
                            dangerCount++;
                        } else {
                            platform.material = greenMat;
                            platform.metadata.type = "safe";
                        }

                        platform.rotate(new Vector3(0, 1, 0), totalArc * Math.PI * 2);
                        platformArc -= partitionArc;
                        totalArc += partitionArc;

                        platformGroup.push(platform);
                    }
                }
            }

            const invisiblePlatform = MeshBuilder.CreateCylinder(
                "invisiblePlatform",
                this.properties.base,
                scene
            );
            invisiblePlatform.position.y = platformPosition.y;
            invisiblePlatform.visibility = 0;
            
            const randomRotation = Math.random() * Math.PI * 2;
            platformGroup.forEach((mesh) => {
                mesh.rotate(new Vector3(0, 1, 0), randomRotation);
                mesh.physicsImpostor = new PhysicsImpostor(mesh, PhysicsImpostor.MeshImpostor, {mass: 0});
                if (mesh.metadata.type == "hole") {
                    mesh.physicsImpostor.physicsBody.collisionResponse = false;
                }

                if (mesh.metadata.arc) {
                    const rotateAngle = mesh.metadata.arc * Math.PI * 2 - randomRotation;

                    mesh.metadata.finalPosition = {
                        x: mesh.position.x + Math.cos(rotateAngle) * this.properties.base.diameterTop * 0.25,
                        z: mesh.position.z + Math.sin(rotateAngle) * this.properties.base.diameterTop * 0.25
                    };

                    const lerpBox = new Animation("disappearAnimLerp", "position", 30,  Animation.ANIMATIONTYPE_VECTOR3);
                    lerpBox.setKeys([
                        {
                            frame: 0,
                            value: new Vector3(
                                mesh.position.x,
                                mesh.position.y,
                                mesh.position.z
                            )
                        },
                        {
                            frame: 15,
                            value: new Vector3(
                                mesh.metadata.finalPosition.x,
                                mesh.position.y,
                                mesh.metadata.finalPosition.z
                            )
                        }
                    ]);

                    const fadeBox = new Animation("disappearAnimFade", "visibility", 30,  Animation.ANIMATIONTYPE_FLOAT);
                    fadeBox.setKeys([
                        {
                            frame: 0,
                            value: mesh.visibility
                        },
                        {
                            frame: 15,
                            value: 0
                        }
                    ]);

                    mesh.animations = [lerpBox, fadeBox];
                }
                

                ball.physicsImpostor?.registerOnPhysicsCollide(mesh.physicsImpostor, () => {
                    if (!mesh.metadata.active) return;

                    const ballVelocY = ball.physicsImpostor?.getLinearVelocity()?.y as number;
                    switch(mesh.metadata.type) {
                        case "safe": {
                            if (ballVelocY <= 1) {
                                this.generateDecal(
                                    ball,
                                    mesh,
                                    invisiblePlatform,
                                    decalMat,
                                    decalFadeAnim,
                                    scene
                                );

                                ball.physicsImpostor?.executeNativeFunction((world, body) => {
                                    body.velocity = new cannon.Vec3(0, this.speedLimit.max, 0);
                                });
                            }
                            break;
                        }

                        case "danger": {
                            if (!this.gameOver) {
                                this.generateDecal(
                                    ball,
                                    mesh,
                                    invisiblePlatform,
                                    decalMat,
                                    decalFadeAnim,
                                    scene
                                );

                                ball.physicsImpostor?.dispose();
                                ball.position.y = mesh.position.y + this.properties.base.height / 2 + this.ballDiameter / 2;
                                this.setupGUI(scene, "Game Over! Tap to restart");
                                this.gameOver = true;
                            }
                            break;
                        }

                        case "hole": {
                            invisiblePlatform.dispose();
                            platformGroup.forEach((innerMesh) => {
                                innerMesh.metadata.active = false;
                                if (innerMesh.physicsImpostor) {
                                    innerMesh.physicsImpostor.physicsBody.collisionResponse = false;
                                }
                                scene.beginAnimation(innerMesh, 0, 20, false, 1, () => {
                                    innerMesh.dispose();
                                });
                            });
                            break;
                        }
                    }
                });
            });

            platformPosition.y += this.gap;
        }
    }

    /**
     * Generates number between min (inclusive) and max (inclusive)
     * @param min Smallest random number possible
     * @param max Largest random number possible
     * @param randomPower Power applied to random number for weighting
     */
    RNGBetween(min: number, max: number, randomPower = 1) {
        return Math.min(min + Math.pow(Math.random(), randomPower) * (max - min), max);
    }

    setupControl(scene: Scene, ball: Mesh) {
        scene.onKeyboardObservable.add((kbInfo) => {
            switch (kbInfo.type) {
                case KeyboardEventTypes.KEYDOWN:
                    console.log("KEY DOWN: ", kbInfo.event.key);
                    break;
                case KeyboardEventTypes.KEYUP:
                    const keyCode = kbInfo.event.keyCode;
                    console.log("KEY UP: ", keyCode);
                    switch (keyCode) {
                        case 82:
                            GameInstanceManager.Instance?.startScene(new FirstScene());
                            break;
                    }
                    break;
            }
        });

        this.touchControlObservable = scene.onPointerObservable.add((pointerInfo) => {
            switch (pointerInfo.type) {
                case PointerEventTypes.POINTERDOWN:
                    this.touching = true;
                    break;

                case PointerEventTypes.POINTERUP:
                    this.touching = false;
                    break;

                case PointerEventTypes.POINTERMOVE:
                    if (this.touching) {
                        const alphaInc = pointerInfo.event.movementX * scene.getEngine().getDeltaTime() / 1000;
                        if (this.camera) {
                            this.camera.alpha -= alphaInc;
                        }

                        this.currentBallAngle -= alphaInc;

                        ball.metadata.x = Math.cos(this.currentBallAngle);
                        ball.metadata.z = Math.sin(this.currentBallAngle);

                        ball.rotate(ball.up, alphaInc);
                    }
                    break;
            }
        });
    }

    generateDecal(
        ball: Mesh, 
        mesh: Mesh, 
        parent: Mesh, 
        decalMat: StandardMaterial, 
        decalFadeAnim: Animation,
        scene: Scene
    ) {
        const decalSize = this.RNGBetween(0.5, 1);
        const decal = MeshBuilder.CreateDecal("decal", parent,  {
            position: new Vector3(
                ball.position.x,
                mesh.position.y + this.properties.base.height / 2,
                ball.position.z
            ),
            size: new Vector3(
                decalSize,
                decalSize,
                0.1,
            )
        });
        decal.material = decalMat;
        decal.rotate(new Vector3(0, 0, 1), Math.random() * Math.PI * 2);
        decal.animations = [decalFadeAnim];

        setTimeout(() => {
            scene.beginAnimation(decal, 0, 60, false, 1, () => {
                decal.dispose();
            });
        }, 1000);

        parent.addChild(decal);

        if (ball.metadata.particleSystem) {
            ball.metadata.particleSystem.manualEmitCount = decalSize * 15;
        }
    }

    setupGUI(scene: Scene, text: string) {
        var advancedTexture = AdvancedDynamicTexture.CreateFullscreenUI("UI", true, scene);
        /**
         * Ideal width is used for texture size reference.
         * E.g. if set to 720, then when the screen's width is only 360 all textures size would be halved
         */
        advancedTexture.idealWidth = 720;
        advancedTexture.renderAtIdealSize = true;

        var textObj = new TextBlock();
        textObj.text = text;
        textObj.color = "white";
        textObj.fontSize = 24;
        advancedTexture.addControl(textObj);    

        if (this.touchControlObservable) {
            scene.onPointerObservable.remove(this.touchControlObservable);
        }

        scene.onPointerObservable.add((pointerInfo) => {
            switch (pointerInfo.type) {
                case PointerEventTypes.POINTERDOWN:
                    GameInstanceManager.Instance?.startScene(new FirstScene());
                    break;
            }
        });
    }

    /**
     * Scene's update function
     */
    public onRender = (scene: Scene) => {
        // if (this.sphere) {
        //     var deltaTimeInMillis = scene.getEngine().getDeltaTime();

        //     const rpm = 10;
        //     this.sphere.rotation.y += ((rpm / 60) * Math.PI * 2 * (deltaTimeInMillis / 1000));
        // }
    }
}